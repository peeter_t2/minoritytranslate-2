package ee.translate.keeleleek.mtpluginframework.spellcheck;


public class StripUtil {

	public static final char FILL = ' ';
	public static final char CONNECT = '_';
	
	public static String strip(String text)
	 {
		// \u00AD
		char[] data = text.toCharArray();
		
		if (data.length == 0) return "";
		
		int tagLvl = 0;
		int tmplLvl = 0;
		int wlinkLvl = 0;
		int linkLvl = 0;
	
		boolean tagContent = false;
		boolean wlinkParameter = false;
		
		for (int i = data.length - 1; i > 0; i--) {
			
			if (data[i] == '\n') continue;

			else if (data[i] == '>') {
				data[i] = FILL;
				tagContent = false;
				tagLvl++;
			}

			else if (data[i] == '/' && data[i - 1] == '<') {
				data[i] = FILL;
				tagContent = true;
			}
			
			else if (data[i] == '<') {
				data[i] = FILL;
				tagLvl--;
			}

			else if (data[i] == '}') {
				data[i] = FILL;
				tmplLvl++;
			}
			
			else if (data[i] == '{') {
				data[i] = FILL;
				tmplLvl--;
			}

			else if (data[i] == ']' && data[i - 1] == ']') {
				data[i] = CONNECT;
				data[i - 1] = CONNECT;
				i--;
				wlinkParameter = true;
				wlinkLvl++;
			}
			
			else if (data[i] == '|' && wlinkParameter) {
				data[i] = FILL;
				wlinkParameter = false;
			}

			else if (data[i] == '[' && data[i - 1] == '[') {
				data[i] = FILL;
				data[i - 1] = FILL;
				i--;
				wlinkParameter = false;
				wlinkLvl--;
			}

			else if (data[i] == ']') {
				data[i] = FILL;
				linkLvl++;
			}
			
			else if (data[i] == '[') {
				data[i] = FILL;
				linkLvl--;
			}

			else if (data[i] == '=' || data[i] == '\'') {
				data[i] = FILL;
			}

			else if (data[i] == '(' || data[i] == ')') {
				data[i] = FILL;
			}

			else if (tmplLvl != 0 || tagLvl != 0 || linkLvl != 0 || tagContent) data[i] = FILL;
			else if (wlinkLvl != 0 && !wlinkParameter) data[i] = FILL;
			else if (wlinkLvl != 0 && !wlinkParameter) data[i] = FILL;
			
		}
		
		if (data[0] == '{' || data[0] == '}') data[0] = FILL;
		else if (data[0] == '[' || data[0] == ']') data[0] = FILL;
		else if (data[0] == '<' || data[0] == '>') data[0] = FILL;
		else if (data[0] == '\'') data[0] = FILL;
		
		return new String(data);
	 }
	
	public static String stripAll(String text)
	 {
		// \u00AD
		char[] data = text.toCharArray();
		
		if (data.length == 0) return "";
		
		int tagLvl = 0;
		int tmplLvl = 0;
		int wlinkLvl = 0;
		int linkLvl = 0;
	
		boolean tagContent = false;
		boolean wlinkParameter = false;
		
		for (int i = data.length - 1; i > 0; i--) {
			
			if (data[i] == '>') {
				data[i] = FILL;
				tagContent = false;
				tagLvl++;
			}

			else if (data[i] == '/' && data[i - 1] == '<') {
				data[i] = FILL;
				tagContent = true;
			}
			
			else if (data[i] == '<') {
				data[i] = FILL;
				tagLvl--;
			}

			else if (data[i] == '}') {
				data[i] = FILL;
				tmplLvl++;
			}
			
			else if (data[i] == '{') {
				data[i] = FILL;
				tmplLvl--;
			}

			else if (data[i] == ']' && data[i - 1] == ']') {
				data[i] = CONNECT;
				data[i - 1] = CONNECT;
				i--;
				wlinkParameter = true;
				wlinkLvl++;
			}
			
			else if (data[i] == '|' && wlinkParameter) {
				data[i] = FILL;
				wlinkParameter = false;
			}

			else if (data[i] == '[' && data[i - 1] == '[') {
				data[i] = FILL;
				data[i - 1] = FILL;
				i--;
				wlinkParameter = false;
				wlinkLvl--;
			}

			else if (data[i] == ']') {
				data[i] = FILL;
				linkLvl++;
			}
			
			else if (data[i] == '[') {
				data[i] = FILL;
				linkLvl--;
			}

			else if (data[i] == '=' || data[i] == '\'') {
				data[i] = FILL;
			}

			else if (data[i] == '(' || data[i] == ')') {
				data[i] = FILL;
			}

			else if (tmplLvl != 0 || tagLvl != 0 || linkLvl != 0 || tagContent) data[i] = FILL;
			else if (wlinkLvl != 0 && !wlinkParameter) data[i] = FILL;
			else if (wlinkLvl != 0 && !wlinkParameter) data[i] = FILL;
			
		}
		
		if (data[0] == '{' || data[0] == '}') data[0] = FILL;
		else if (data[0] == '[' || data[0] == ']') data[0] = FILL;
		else if (data[0] == '<' || data[0] == '>') data[0] = FILL;
		else if (data[0] == '\'') data[0] = FILL;
		
		return new String(data);
	 }

	
}
