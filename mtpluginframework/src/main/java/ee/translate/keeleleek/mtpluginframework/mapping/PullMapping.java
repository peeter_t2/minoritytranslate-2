package ee.translate.keeleleek.mtpluginframework.mapping;

/**
 * Used to mediate interaction between the user and article chopup. 
 */
public interface PullMapping {

	/**
	 * Gets the mapping group.
	 * Mapping group is used to arrange similar mappings in the mapping dialog.
	 * 
	 * @return mapping group
	 */
	public String getGroup();
	
	/**
	 * Gets the mapping source.
	 * Source indicates what is being translated.
	 * 
	 * @return mapping source
	 */
	public String getSource();
	
	/**
	 * Gets the mapping destination.
	 * Destination specifies the value that is being mapped.
	 * 
	 * @return destination value
	 */
	public String getDestination();
	
	/**
	 * Updates the destination.
	 * 
	 * @param destination new destination
	 */
	public void setDestination(String destination);
	
	
	/**
	 * Indicates if the mapping was touched.
	 * 
	 * @return true if touched
	 */
	public boolean isTouched();
	
}
