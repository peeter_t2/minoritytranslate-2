package ee.translate.keeleleek.mtpluginframework.chopup;

public class TagContentElement extends ArticleElement {

	private String code;
	
	public TagContentElement() {
		super(Type.TAG_CONTENT);
	}
	
	@Override
	public void init(String text) {
		this.code = text;
	}
	
	@Override
	protected String asCode() {
		return code;
	}
	
}
