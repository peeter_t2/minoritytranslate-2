package ee.translate.keeleleek.mtpluginframework.spellcheck;

import java.util.Collection;

import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugin;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;

/**
 * Interface for spell check plugins.
 */
public interface SpellerPlugin extends MinorityTranslatePlugin {
	
	/**
	 * Gets plugin version.
	 * 
	 * @return plugin version
	 */
	public PluginVersion getSpellerVersion();
	
	/**
	 * Spell checks.
	 * 
	 * @param langCode language code
	 * @param text text to spell check
	 * @param misspells misspell collection to collect to
	 * @return true if successful
	 * @throws Exception when spell check fails
	 */
	public boolean spellCheck(String langCode, String text, Collection<Misspell> misspells) throws Exception;
	
	
}
