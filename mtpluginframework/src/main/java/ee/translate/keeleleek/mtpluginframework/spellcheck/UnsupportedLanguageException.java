package ee.translate.keeleleek.mtpluginframework.spellcheck;

/**
 * Thrown when the language pair is not supported.
 */
public class UnsupportedLanguageException extends Exception {

	private static final long serialVersionUID = 4581062922695095288L;
	
	private String langCode;
	private String pluginName;
	
	
	// INIT
	public UnsupportedLanguageException(String langCode, String pluginName) {
		super("Language " + langCode + " is not supported by the " + pluginName + " plugin");
		this.langCode = langCode;
		this.pluginName = pluginName;
	}

	
	// DETAILS
	public String getLangCode() {
		return langCode;
	}
	
	public String getPluginName() {
		return pluginName;
	}
	
	
}
