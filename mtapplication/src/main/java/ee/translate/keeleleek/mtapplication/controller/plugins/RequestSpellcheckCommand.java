package ee.translate.keeleleek.mtapplication.controller.plugins;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.common.requests.SpellCheckRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;
import ee.translate.keeleleek.mtpluginframework.spellcheck.UnsupportedLanguageException;

public class RequestSpellcheckCommand extends SimpleCommand {
	
	private static Logger LOGGER = LoggerFactory.getLogger(RequestSpellcheckCommand.class);
	
	@Override
	public void execute(INotification notification)
	 {
		final SpellCheckRequest request = (SpellCheckRequest) notification.getBody();
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					
					MinorityTranslateModel.speller().requestSpellCheck(request);
					
				} catch (final UnsupportedLanguageException e) {
					MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
						@Override
						public void run() {
							
							PullPlugin plugin = MinorityTranslateModel.pull().getPlugin(e.getPluginName());
							String pluginName = null;
							if (plugin != null) pluginName = plugin.getName(MinorityTranslateModel.preferences().getGUILangCode());
							if (pluginName == null) pluginName = e.getPluginName();
							
							String content = Messages.getString("messages.spell.check.failed.content.language.not.supported");
							content = content.replace("#", pluginName);
							content = content.replace("#", e.getLangCode());
							
							DialogFactory.dialogs().showError(Messages.getString("messages.spell.check.failed.header"), content);
							
						}
					});
				}
				catch (final Exception e) {
					MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
						@Override
						public void run() {
							
							LOGGER.error("Spell check failed", e);
							DialogFactory.dialogs().showError(Messages.getString("messages.spell.check.failed.header"), Messages.getString("messages.spell.check.failed.content.exception").replace("#", e.toString()));
							
						}
					});
				}
			}
		});
		thread.start();
	 }
	
}
