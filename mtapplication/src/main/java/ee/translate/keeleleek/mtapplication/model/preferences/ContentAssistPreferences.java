package ee.translate.keeleleek.mtapplication.model.preferences;


public class ContentAssistPreferences {

	private Boolean insertSingleAuto;
	private Boolean searchLinks;
	private Boolean translateWikilinks;
	private Boolean translateTemplates;
	
	
	// INIT
	public ContentAssistPreferences()
	 {
		insertSingleAuto = true;
		searchLinks = true;
		translateWikilinks = true;
		translateTemplates = true;
	 }
	
	
	public ContentAssistPreferences(Boolean insertSingleAuto, Boolean searchLinks, Boolean translateWikilinks, Boolean translateTemplates)
	 {
		this.insertSingleAuto = insertSingleAuto;
		this.searchLinks = searchLinks;
		this.translateWikilinks = translateWikilinks;
		this.translateTemplates = translateTemplates;
	 }


	public ContentAssistPreferences(ContentAssistPreferences assist)
	 {
		this.insertSingleAuto = assist.insertSingleAuto;
		this.searchLinks = assist.searchLinks;
		this.translateWikilinks = assist.translateWikilinks;
		this.translateTemplates = assist.translateTemplates;
	 }
	

	// PREFERENCES
	public Boolean getInsertSingleAuto() {
		return insertSingleAuto;
	}
	
	public void setInsertSingleAuto(Boolean insertSingleAuto) {
		this.insertSingleAuto = insertSingleAuto;
	}
	
	
	public Boolean getSearchLinks() {
		return searchLinks;
	}
	
	public void setSearchLinks(Boolean searchLinks) {
		this.searchLinks = searchLinks;
	}
	
	public Boolean getTranslateWikilinks() {
		return translateWikilinks;
	}
	
	public Boolean getTranslateTemplates() {
		return translateTemplates;
	}


	// UTILITY
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((insertSingleAuto == null) ? 0 : insertSingleAuto.hashCode());
		result = prime * result + ((searchLinks == null) ? 0 : searchLinks.hashCode());
		result = prime * result + ((translateTemplates == null) ? 0 : translateTemplates.hashCode());
		result = prime * result + ((translateWikilinks == null) ? 0 : translateWikilinks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ContentAssistPreferences other = (ContentAssistPreferences) obj;
		if (insertSingleAuto == null) {
			if (other.insertSingleAuto != null) return false;
		} else if (!insertSingleAuto.equals(other.insertSingleAuto)) return false;
		if (searchLinks == null) {
			if (other.searchLinks != null) return false;
		} else if (!searchLinks.equals(other.searchLinks)) return false;
		if (translateTemplates == null) {
			if (other.translateTemplates != null) return false;
		} else if (!translateTemplates.equals(other.translateTemplates)) return false;
		if (translateWikilinks == null) {
			if (other.translateWikilinks != null) return false;
		} else if (!translateWikilinks.equals(other.translateWikilinks)) return false;
		return true;
	}

	
}
