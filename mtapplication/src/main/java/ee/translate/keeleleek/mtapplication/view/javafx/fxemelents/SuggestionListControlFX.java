package ee.translate.keeleleek.mtapplication.view.javafx.fxemelents;

import ee.translate.keeleleek.mtapplication.model.lists.SuggestionList;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SuggestionListControlFX implements SuggestionListFX {

	private final StringProperty name;
	private final BooleanProperty enabled;
	
	private final String fullName;
	
	
	public SuggestionListControlFX(SuggestionList suggestList, boolean enable)
	 {
		this.name = new SimpleStringProperty(adjustName(suggestList.getName()));
		this.enabled = new SimpleBooleanProperty(enable);
		this.fullName = suggestList.getName();
	 }

	public StringProperty name() {
		return name;
	}
	
	public BooleanProperty enabled() {
		return enabled;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	@Override
	public String toString() {
		return name.get();
	}
	

	private String adjustName(String string) {
		string = trimSubpages(string);
		string = trimNamespace(string);
		string = replaceUnderscores(string);
		return string;
	}
	
	private String trimSubpages(String string) {
		int i = string.lastIndexOf('/');
		if (i != -1) string = string.substring(i + 1);
		return string;
	}

	private String trimNamespace(String string) {
		int i = string.lastIndexOf(':');
		if (i != -1) string = string.substring(i + 1);
		return string;
	}

	private String replaceUnderscores(String string) {
		return string.replace('_', ' ');
	}
	
}
