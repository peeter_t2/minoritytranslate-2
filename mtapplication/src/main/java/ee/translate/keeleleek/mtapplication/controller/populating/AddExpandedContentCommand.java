package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.WikiRequest;

public class AddExpandedContentCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		WikiRequest request = (WikiRequest) notification.getBody();
		MinorityTranslateModel.queuer().requestExpanded(request);
	 }
	
}
