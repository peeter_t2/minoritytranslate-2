package ee.translate.keeleleek.mtapplication.model.preferences;


public class TemplateMapping {

	private String srcTemplate = "";
	private String srcParameter = "";
	private String dstParameter = "";
	private EntryFilter filter = new EntryFilter();
	

	public TemplateMapping() {
		
	}
	
	public TemplateMapping(String srcTemplate, String srcParameter, String srcLangCode, String dstParameter, String dstLangCode) {
		this.srcTemplate = srcTemplate;
		this.srcParameter = srcParameter;
		this.dstParameter = dstParameter;
		this.filter = new EntryFilter(srcLangCode, dstLangCode);
	}
	
	public TemplateMapping(TemplateMapping other) {
		this.srcTemplate = other.srcTemplate;
		this.srcParameter = other.srcParameter;
		this.dstParameter = other.dstParameter;
		this.filter = other.filter;
	}

	public String getSrcTemplate() {
		return srcTemplate;
	}

	public String getSrcParameter() {
		return srcParameter;
	}

	public String getDstParameter() {
		return dstParameter;
	}

	public EntryFilter getFilter() {
		return filter;
	}
	
	public String getSrcLang() {
		return filter.getSrcLangRegex();
	}

	public String getDstLang() {
		return filter.getDstLangRegex();
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dstParameter == null) ? 0 : dstParameter.hashCode());
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((srcParameter == null) ? 0 : srcParameter.hashCode());
		result = prime * result + ((srcTemplate == null) ? 0 : srcTemplate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemplateMapping other = (TemplateMapping) obj;
		if (dstParameter == null) {
			if (other.dstParameter != null)
				return false;
		} else if (!dstParameter.equals(other.dstParameter))
			return false;
		if (filter == null) {
			if (other.filter != null)
				return false;
		} else if (!filter.equals(other.filter))
			return false;
		if (srcParameter == null) {
			if (other.srcParameter != null)
				return false;
		} else if (!srcParameter.equals(other.srcParameter))
			return false;
		if (srcTemplate == null) {
			if (other.srcTemplate != null)
				return false;
		} else if (!srcTemplate.equals(other.srcTemplate))
			return false;
		return true;
	}

	
}
