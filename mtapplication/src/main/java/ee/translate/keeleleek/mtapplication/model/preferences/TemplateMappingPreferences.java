package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TemplateMappingPreferences {

	private ArrayList<TemplateMapping> mappings;

	
	// INIT
	public TemplateMappingPreferences()
	 {
		mappings = new ArrayList<>();
	 }
	

	public TemplateMappingPreferences(Collection<TemplateMapping> mappings)
	 {
		this.mappings = new ArrayList<>(mappings);
	 }

	public TemplateMappingPreferences(TemplateMappingPreferences other)
	 {
		mappings = new ArrayList<>();
		
		for (TemplateMapping template : other.mappings) {
			this.mappings.add(new TemplateMapping(template));
		}
	 }
	

	// PREFERENCE VALUES
	public int getMappingCount() {
		return mappings.size();
	}
	
	public TemplateMapping getMapping(int i) {
		return mappings.get(i);
	}
	
	public List<TemplateMapping> findMappings(String srcLangCode, String dstLangCode, String srcTemplate)
	 {
		ArrayList<TemplateMapping> result = new ArrayList<>();
		for (TemplateMapping mapping : mappings) {
			if (!mapping.getSrcTemplate().equals(srcTemplate)) continue;
			if (!mapping.getFilter().isAccept(srcLangCode, dstLangCode)) continue;
			result.add(mapping);
		}
		
		return result;
	 }
	
	
	// HELPERS
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mappings == null) ? 0 : mappings.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		TemplateMappingPreferences other = (TemplateMappingPreferences) obj;
		if (mappings == null) {
			if (other.mappings != null) return false;
		} else if (!mappings.equals(other.mappings)) return false;
		return true;
	}

	
}
