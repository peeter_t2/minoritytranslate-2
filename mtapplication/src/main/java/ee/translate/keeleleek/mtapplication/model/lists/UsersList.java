package ee.translate.keeleleek.mtapplication.model.lists;

public class UsersList implements SuggestionList {

	private String langCode;
	private String title;
	private String group;
	
	
	public String getLangCode() {
		return langCode;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getName() {
		return title + " (" + langCode + ")";
	}
	
	@Override
	public String getGroup() {
		if (group == null || group.isEmpty()) return GROUP_UNGROUPED;
		return group;
	}
	
	
	@Override
	public String toString() {
		return "{" + langCode + ":" + title + "}";
	}
	
}
