package ee.translate.keeleleek.mtapplication.model.plugins;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugin;
import ee.translate.keeleleek.mtpluginframework.MinorityTranslatePlugins;
import ee.translate.keeleleek.mtpluginframework.PluginVersion;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;
import ee.translate.keeleleek.mtpluginframework.spellcheck.SpellerPlugin;

public class PluginsProxy extends Proxy {

	public final static String NAME = "{E2E3576C-956C-4D75-99EF-D05A4FF51B5E}";
	
	private static String PLUGINS_DIRECTORY = MinorityTranslate.ROOT_PATH + File.separator + "plugins";
	
	private static PluginVersion SPELL_CHECK_VERSION = new PluginVersion(1, 0);
	private static PluginVersion PULL_VERSION = new PluginVersion(1, 0);
	
	private static Logger LOGGER = LoggerFactory.getLogger(PluginsProxy.class);

	private ArrayList<MinorityTranslatePlugin> plugins = new ArrayList<>();
	
	private MinorityTranslatePlugins pluginsCallback = new MinorityTranslatePlugins() {

		@Override
		public SpellerPlugin findSpellerPlugin(String pluginName) {
			return MinorityTranslateModel.speller().getPlugin(pluginName);
		}
		
		@Override
		public PullPlugin findPullPlugin(String pluginName) {
			return MinorityTranslateModel.pull().getPlugin(pluginName);
		}
		
		@Override
		public String getStringMessage(String key) {
			return Messages.getString(key);
		}
		
		public Path getRootPath() {
			return Paths.get(MinorityTranslate.ROOT_PATH);
		};
		
		public Path getPluginsPath() {
			return Paths.get(PLUGINS_DIRECTORY);
		};
		
	};
	
	
	// INIT
	public PluginsProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		try{	  
			// filter jars
			File pluginsDir = new File(PLUGINS_DIRECTORY);
			if (!pluginsDir.isDirectory()) pluginsDir.mkdirs();
			File[] pluginsFiles = pluginsDir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".jar");
				}
			});

			// premade plugins
			MinorityTranslateModel.pull().addPlugin(new CopyReplacePastePlugin());
			MinorityTranslateModel.speller().addPlugin(new DisabledSpellerPlugin());
			
			// load plugins
			for (File file : pluginsFiles) {
				
				LOGGER.info("Loading " + file);
				
				JarFile jar = null;
				try {
					jar  = new JarFile(file);
					Manifest manifest = jar.getManifest();
	                Attributes attributes = manifest.getMainAttributes();
	                
	                String mainClass = attributes.getValue("Main-Class");
	                if (mainClass == null) throw new Exception("Main class not defined");
	                
	                URLClassLoader child = new URLClassLoader(new URL[]{file.toURI().toURL()}, this.getClass().getClassLoader());
					Class<?> pluginClass = Class.forName (mainClass, true, child);
					Object instance = pluginClass.newInstance();

					boolean success = false;
					
					// spell check plugins
					if (instance instanceof SpellerPlugin) {
						SpellerPlugin plugin = (SpellerPlugin) instance;
						if (!plugin.getSpellerVersion().isCompatibleWith(SPELL_CHECK_VERSION)) {
							LOGGER.warn("Plugin " + plugin.getName() + " " + plugin.getSpellerVersion() + " is incompatible!");
							continue;
						}
						success = true;
					}

					// pull plugins
					if (instance instanceof PullPlugin) {
						PullPlugin plugin = (PullPlugin) instance;
						if (!plugin.getPullVersion().isCompatibleWith(PULL_VERSION)) {
							LOGGER.warn("Plugin " + plugin.getName() + " " + plugin.getPullVersion() + " is incompatible!");
							continue;
						}
						success = true;
					}
					
					if (success) {
						
						MinorityTranslatePlugin plugin = (MinorityTranslatePlugin) instance;
						plugin.setup(pluginsCallback);
						
						plugins.add(plugin);
						
						if (plugin instanceof SpellerPlugin) {
							SpellerPlugin spellerPlugin = (SpellerPlugin) plugin;
							MinorityTranslateModel.speller().addPlugin(spellerPlugin);
							LOGGER.info("Successfully loaded " + spellerPlugin.getName() + " " + spellerPlugin.getSpellerVersion() + " speller plugin from " + file);
						}
						
						if (plugin instanceof PullPlugin) {
							PullPlugin pullPlugin = (PullPlugin) plugin;
							MinorityTranslateModel.pull().addPlugin(pullPlugin);
							LOGGER.info("Successfully loaded " + pullPlugin.getName() + " " + pullPlugin.getPullVersion() + " pull plugin from " + file);
						}
						
					} else {
						throw new Exception("Invalid plugin");
					}
					
				} catch (Exception e) {
					LOGGER.error("Failed to load plugin " + file.toString(), e);
				} finally {
					if (jar != null) jar.close();
				}
				
			}
			
		  } catch (Exception e) {
			  LOGGER.error("Failed to read plugins", e);
		  }
	 }
	
	@Override
	public void onRemove()
	 {
		
	 }
	
	public void close()
	 {
		// close all plugins
		for (MinorityTranslatePlugin plugin : plugins) {
			try {
				LOGGER.info("Closing " + plugin.getName() + " plugin");
				plugin.close();
			} catch (Exception e) {
				LOGGER.error("Failed to close " + plugin.getName() + " plugin", e);
			}
		}
	 }
	
	
}
