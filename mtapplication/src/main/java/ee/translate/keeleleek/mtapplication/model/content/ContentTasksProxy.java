package ee.translate.keeleleek.mtapplication.model.content;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import net.sourceforge.jwbf.core.contentRep.Article;

public class ContentTasksProxy extends Proxy implements Runnable {

	public final static String NAME = "{CE4889A3-69B6-40CC-A7A0-9F9B4011105E}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ContentTasksProxy.class);
	
	public final static long MAX_TASKS = 50;
	public final static long TIMEOUT = 250*0 + 1000;
	
	private ConcurrentLinkedQueue<Reference> titleChecksTasks = new ConcurrentLinkedQueue<>();
	
	private boolean run = false;

	
	// INIT
	public ContentTasksProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister() {
		run = true;
		
		startThread();
	}
	
	@Override
	public void onRemove() {
		run = false;
	}

	private void startThread()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// QUEUE
	public void queueTitleCheck(Reference ref)
	 {
		if (titleChecksTasks.contains(ref)) return;
		titleChecksTasks.offer(ref);
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				workTitleChecks();
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (IllegalStateException e) {
			LOGGER.warn("Failed to upload content: " + e.getMessage() + "!");
			startThread();
		}
	 }
	
	private void workTitleChecks()
	 {
		MinorityArticle marticle = ContentUtil.findArticle(MinorityTranslateModel.content(), TitleStatus.CHECKING_REQUIRED);
		
		if (marticle != null) {

			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_TITLE_CHECK_BUSY, true);
			
			Reference ref = marticle.getRef();
			
			MinorityTranslateModel.content().changeTitleStatus(ref, TitleStatus.CHECKING);
			
			String title = marticle.getTitle();
			String langCode = ref.getLangCode();

			if (title.isEmpty()) {
				
				MinorityTranslateModel.content().changeTitleStatus(ref, TitleStatus.OK);
			
			} else {
				
				Article article = MinorityTranslateModel.bots().retrieveArticle(langCode, title);
				
				if (!article.getText().isEmpty()) {
					MinorityTranslateModel.content().changeTitleStatus(ref, TitleStatus.CONFLICT);
				} else {
					MinorityTranslateModel.content().changeTitleStatus(ref, TitleStatus.OK);
				}
				
			}
			
		}
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_TITLE_CHECK_BUSY, false);
	 }

	
}
