package ee.translate.keeleleek.mtapplication.model.serialising;

import java.lang.reflect.Type;
import java.util.regex.Pattern;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ee.translate.keeleleek.mtapplication.model.content.Namespace;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;

public class WikiReferenceSerializer implements JsonSerializer<WikiReference>, JsonDeserializer<WikiReference> {

    @Override
    public JsonElement serialize(WikiReference ref, Type type, JsonSerializationContext jsc)
     {
        JsonObject jo = new JsonObject();
        jo.addProperty("langCode", ref.getLangCode());
        jo.addProperty("wikiTitle", ref.getWikiTitle());
        jo.addProperty("namespace", ref.getNamespace().ordinal());
        return jo;
     }

	@Override
	public WikiReference deserialize(JsonElement je, Type t, JsonDeserializationContext jdc) throws JsonParseException
	 {
		WikiReference ref;
        JsonObject jo;

        if (je.isJsonObject()) {
        	
            jo = je.getAsJsonObject();
            
            JsonElement namespaceJO = jo.get("namespace");
            int ns = 0;
            if (namespaceJO != null) ns = namespaceJO.getAsInt();
            
            ref = new WikiReference(jo.get("langCode").getAsString(), Namespace.find(ns), jo.get("wikiTitle").getAsString());
        
        } else {
        	
            String jso = je.getAsString();
            
            System.out.println(jso);
            
            String[] split = jso.split(Pattern.quote("&"), 3);
            
            int ns = 0;
            if (split.length == 3) ns = Integer.parseInt(split[2]);
            
            ref = new WikiReference(split[0], Namespace.find(ns), split[1]);
            
        }

        return ref;
	 }
	
}
