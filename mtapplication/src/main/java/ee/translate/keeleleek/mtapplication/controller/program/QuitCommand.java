package ee.translate.keeleleek.mtapplication.controller.program;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class QuitCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		sendNotification(Notifications.QUITTING_COMMIT);

		sendNotification(Notifications.PREFERENCES_SAVE);
		sendNotification(Notifications.SAVE_WIKIS);
		sendNotification(Notifications.SAVE_LISTS);
		
		MinorityTranslateModel.lists().cancel();
		MinorityTranslateModel.connection().stop();
		MinorityTranslateModel.plugins().close();
	 }
	
}
