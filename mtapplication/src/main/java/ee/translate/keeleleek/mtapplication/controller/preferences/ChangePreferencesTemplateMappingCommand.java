package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.preferences.TemplateMappingPreferences;

public class ChangePreferencesTemplateMappingCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		TemplateMappingPreferences preferences = (TemplateMappingPreferences) notification.getBody();
		
		MinorityTranslateModel.preferences().changeTemplateMapping(preferences);
	 }
	
}
