package ee.translate.keeleleek.mtapplication.model.preferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.autocomplete.AutocompleteChoice;

public class Insert {

	private String name;
	private String trigger;
	private String insert;
	
	public Insert(String name, String trigger, String insert) {
		this.name = name;
		this.trigger = trigger;
		this.insert = insert;
	}

	public Insert(Insert other) {
		this.name = other.name;
		this.trigger = other.trigger;
		this.insert = other.insert;
	}

	
	public AutocompleteChoice toChoice(String prefix, String langCode)
	 {
		String trigger = this.trigger;
		String insert = this.insert;
		String name = this.name;
		
		trigger = MinorityTranslateModel.processer().variableProcess(trigger, langCode);
		insert = MinorityTranslateModel.processer().variableProcess(insert, langCode);
		
		// regex approach
		if (trigger.contains("#") || trigger.contains("~")) {
			
			String rgx = trigger;
			rgx = "^.*?\\Q" + rgx; // treat as one (start)
			rgx = rgx + "\\E$"; // treat as one (end)
			rgx = rgx.replace("#", "\\E(.+?)\\Q"); // capture params
			rgx = rgx.replace("#", "\\E(?:.*?)\\Q"); // anything between
			rgx = rgx.replace("\\Q\\E", ""); // clean

			Pattern pattern = Pattern.compile(rgx);
			Matcher matcher = pattern.matcher(prefix);
			
			if (!matcher.find()) return null;
			
			for (int i = 0; i < matcher.groupCount(); i++) {
				insert = insert.replaceFirst("#", matcher.group(i + 1));
				name = name.replaceFirst("#", matcher.group(i + 1));
			}
			
			int caret = 0;
			int c = insert.indexOf(InsertsPreferences.CARET_SYMBOL);
			if (c != -1) {
				caret = insert.length() - c - 1;
				insert = insert.replace(new Character(InsertsPreferences.CARET_SYMBOL).toString(), "");
			}
			
			return new AutocompleteChoice(name, insert, "", prefix.length(), caret);
			
		}
		
		// plain startsWith
		else {

			int caret = 0;
			int c = insert.indexOf(InsertsPreferences.CARET_SYMBOL);
			if (c != -1) {
				caret = insert.length() - c - 1;
				insert = insert.replace(new Character(InsertsPreferences.CARET_SYMBOL).toString(), "");
			}
			
			if (!trigger.startsWith(prefix)) return null;
			return new AutocompleteChoice(name, insert, "", prefix.length(), caret);
			
		}
		
	 }
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	public String getInsert() {
		return insert;
	}

	public void setInsert(String insert) {
		this.insert = insert;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((insert == null) ? 0 : insert.hashCode());
		result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Insert other = (Insert) obj;
		if (name == null) {
			if (other.name != null) return false;
		} else if (!name.equals(other.name)) return false;
		if (insert == null) {
			if (other.insert != null) return false;
		} else if (!insert.equals(other.insert)) return false;
		if (trigger == null) {
			if (other.trigger != null) return false;
		} else if (!trigger.equals(other.trigger)) return false;
		return true;
	}
	
	
}
