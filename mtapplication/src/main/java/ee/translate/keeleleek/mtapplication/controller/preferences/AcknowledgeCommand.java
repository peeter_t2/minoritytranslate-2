package ee.translate.keeleleek.mtapplication.controller.preferences;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class AcknowledgeCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String name = (String) notification.getBody();
		MinorityTranslateModel.preferences().acknowledgeQuickStart(name);
	 }
	
}
