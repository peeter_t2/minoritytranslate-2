package ee.translate.keeleleek.mtapplication.model.processing.processers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.QidMeta;
import ee.translate.keeleleek.mtapplication.model.content.Reference;

public class OldLanglinksProcesser {

	public boolean process(MinorityArticle article)
	 {
		Reference ref = article.getRef();
		String text = article.getText();
		
		// meta
		QidMeta qidMeta = MinorityTranslateModel.content().getQidMeta(ref.getQid());
		if (qidMeta == null) return false;
		
		// already linked
		Pattern pCheck = Pattern.compile(Pattern.quote("[[") + "\\w+:\\w+" + Pattern.quote("]]"));
		Matcher mCheck = pCheck.matcher(text);
		if (mCheck.find()) return false;
		
		// Add links
		HashMap<String, String> langlinks = qidMeta.getLanglinks();
		
		ArrayList<String> langCodes = new ArrayList<>(langlinks.keySet());
		Collections.sort(langCodes);
		
		StringBuilder str = new StringBuilder();
		for (String langCode : langCodes) {
			if (str.length() > 0) str.append('\n');
			str.append("[[" + langCode + ':' + langlinks.get(langCode) + "]]");
		}
		
		text+= "\n\n" + str;
		
		MinorityTranslateModel.content().changeText(ref, text);
		
		return true;
	 }
	
	
	
}
