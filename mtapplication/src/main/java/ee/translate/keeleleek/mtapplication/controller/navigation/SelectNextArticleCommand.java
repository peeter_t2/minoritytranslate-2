package ee.translate.keeleleek.mtapplication.controller.navigation;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class SelectNextArticleCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		MinorityTranslateModel.content().changeNextQid();
	 }
	
}
