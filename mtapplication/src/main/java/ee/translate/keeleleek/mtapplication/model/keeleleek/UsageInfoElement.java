package ee.translate.keeleleek.mtapplication.model.keeleleek;

import java.util.HashMap;

import com.google.gson.Gson;

import ee.translate.keeleleek.mtapplication.model.preferences.PreferencesProxy.Proficiency;


public class UsageInfoElement {

	private String title;
	private String revisionId;
	private String oldId;
	private String langCode;
	private String md5;
	private String tag;
	private String srcLangCode;
	
	private HashMap<String, String> fromIds;
	private HashMap<String, String> toIds;
	
	private HashMap<String, Proficiency> proficiencies;
	
	private Long readyAt;
	private Long savedAt;
	
	private Boolean oneToOne;
	
	private String[] filters;
	
	private String version;
	
	private HashMap<String, String> systemProperties;
	
	
	// INITIATION:
	public UsageInfoElement() {
		
	}
	
	
	// CONVERSION:
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	

	// ACCESSORS:
	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the revisionId.
	 * 
	 * @return the revisionId
	 */
	public String getRevisionId() {
		return revisionId;
	}

	/**
	 * Sets the revisionId.
	 * 
	 * @param revisionId the revisionId to set
	 */
	public void setRevisionId(String revId) {
		this.revisionId = revId;
	}

	/**
	 * Gets the oldId.
	 * 
	 * @return the oldId
	 */
	public String getOldId() {
		return oldId;
	}

	/**
	 * Sets the oldId.
	 * 
	 * @param oldId the oldId to set
	 */
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	/**
	 * Gets the md5.
	 * 
	 * @return the md5
	 */
	public String getMd5() {
		return md5;
	}

	/**
	 * Sets the md5.
	 * 
	 * @param md5 the md5 to set
	 */
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getTag() {
		return tag;
	}
	
	public void setSrcLangCode(String srcLangCode) {
		this.srcLangCode = srcLangCode;
	}
	
	public String getSrcLangCode() {
		return srcLangCode;
	}
	
	/**
	 * Sets tag.
	 * 
	 * @param tag tag
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	/**
	 * Gets the readyAt.
	 * 
	 * @return the readyAt
	 */
	public Long getReadyAt() {
		return readyAt;
	}

	/**
	 * Sets the readyAt.
	 * 
	 * @param readyAt the readyAt to set
	 */
	public void setReadyAt(Long readyAt) {
		this.readyAt = readyAt;
	}

	/**
	 * Gets the savedAt.
	 * 
	 * @return the savedAt
	 */
	public Long getSavedAt() {
		return savedAt;
	}

	/**
	 * Sets the savedAt.
	 * 
	 * @param savedAt the savedAt to set
	 */
	public void setSavedAt(Long savedAt) {
		this.savedAt = savedAt;
	}

	/**
	 * Gets the oneToOne.
	 * 
	 * @return the oneToOne
	 */
	public Boolean getOneToOne() {
		return oneToOne;
	}

	/**
	 * Sets the oneToOne.
	 * 
	 * @param oneToOne the oneToOne to set
	 */
	public void setOneToOne(Boolean oneToOne) {
		this.oneToOne = oneToOne;
	}
	
	public HashMap<String, Proficiency> getProficiencies() {
		return proficiencies;
	}
	
	public void setProficiencies(HashMap<String, Proficiency> proficiencies) {
		this.proficiencies = proficiencies;
	}
	
	/**
	 * Gets the filters.
	 * 
	 * @return the filters
	 */
	public String[] getFilters() {
		return filters;
	}

	/**
	 * Sets the filters.
	 * 
	 * @param filters the filters to set
	 */
	public void setFilters(String[] filters) {
		this.filters = filters;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the systemProperties.
	 * 
	 * @return the systemProperties
	 */
	public HashMap<String, String> getSystemProperties() {
		return systemProperties;
	}

	/**
	 * Sets the systemProperties.
	 * 
	 * @param systemProperties the systemProperties to set
	 */
	public void setSystemProperties(HashMap<String, String> systemProperties) {
		this.systemProperties = systemProperties;
	}

	/**
	 * Gets the fromIds.
	 * 
	 * @return the fromIds
	 */
	public HashMap<String, String> getFromIds() {
		return fromIds;
	}

	/**
	 * Sets the fromIds.
	 * 
	 * @param fromIds the fromIds to set
	 */
	public void setFromIds(HashMap<String, String> fromIds) {
		this.fromIds = fromIds;
	}

	/**
	 * Gets the toIds.
	 * 
	 * @return the toIds
	 */
	public HashMap<String, String> getToIds() {
		return toIds;
	}

	/**
	 * Sets the toIds.
	 * 
	 * @param toIds the toIds to set
	 */
	public void setToIds(HashMap<String, String> toIds) {
		this.toIds = toIds;
	}


	
	/**
	 * Gets the langCode.
	 * 
	 * @return the langCode
	 */
	public String getLangCode() {
		return langCode;
	}


	/**
	 * Sets the langCode.
	 * 
	 * @param langCode the langCode to set
	 */
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	
}
