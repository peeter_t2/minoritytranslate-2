package ee.translate.keeleleek.mtapplication.model.requests;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.List;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.common.requests.LookupRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SymbolsExportRequest;
import ee.translate.keeleleek.mtapplication.common.requests.SymbolsImportRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.preferences.Lookup;
import ee.translate.keeleleek.mtapplication.model.preferences.Symbol;
import ee.translate.keeleleek.mtapplication.model.processing.ProcessingUtil;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class RequestsProxy extends Proxy {

	public final static String NAME = "{0D1A5A9E-2F58-4E5B-BB22-A6BA80BFC45E}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(RequestsProxy.class);
	
	private static String SEARCH = "<search>";
	private static String MERRIAM_WEBSTER_DICTIONARY_KEY = "bbadc51e-5646-4b82-a17e-2bb8c1bed850"; // only for MinorityTranslate
	private static String MERRIAM_WEBSTER_THESAURUS_KEY = "ebc2ab54-dc8b-4d2d-96ed-f974cc143138"; // only for MinorityTranslate
	
	
	// INIT
	public RequestsProxy() {
		super(NAME, null);
	}


	public void fulfill(Object request)
	 {
		Object response = null;
		
		if (request instanceof SymbolsImportRequest) response = fulfill((SymbolsImportRequest)request);
		else if (request instanceof SymbolsExportRequest) response = fulfill((SymbolsExportRequest)request);
		else if (request instanceof LookupRequest) response = fulfill((LookupRequest)request);
		else LOGGER.error("Invalid request " + request);
		
		if (response != null) getFacade().sendNotification(Notifications.REQUEST_FULFILLED, response);
	 }

	
	// SYMBOLS
	public Object fulfill(SymbolsImportRequest request)
	 {
		Path path = request.getPath();
		
		String json;
		try {
			json = FileUtil.read(path);
		} catch (IOException e) {
			LOGGER.error("Failed to read symbols", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.symbols.import.error.header"), Messages.getString("messages.symbols.import.error.failed.to.read").replaceFirst("#path", path.toString()).replaceFirst("#message", e.getClass().getSimpleName() + ": " + e.getMessage()));
			return null;
		}
		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
		builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
		Gson gson = builder.create();
		
		try {
			
			Type type = new TypeToken<List<Symbol>>(){}.getType();
			List<Symbol> symbolList = gson.fromJson(json, type);
			
			return request.fulfill(symbolList);
			
		} catch (JsonSyntaxException e) {
			LOGGER.error("Failed to parse symbols", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.symbols.import.error.header"), Messages.getString("messages.symbols.import.error.failed.to.parse").replaceFirst("#path", path.toString()).replaceFirst("#message", e.getClass().getSimpleName() + ": " + e.getMessage()));
			return null;
		}
	 }

	public Object fulfill(SymbolsExportRequest request)
	 {
		Path path = request.getPath();
		
		try {
			
			GsonBuilder builder = new GsonBuilder();
			Gson gson = builder.create();
			String json = gson.toJson(request.getSymbols());
			FileUtil.write(path, json);
			
			return request.fulfill();
			
		} catch (IOException e) {
			LOGGER.error("Failed to export symbols", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.symbols.export.error.header"), Messages.getString("messages.symbols.export.error.failed.to.write").replaceFirst("#path", path.toString()).replaceFirst("#message",e.getMessage()));
			return null;
		}
	 }

	
	// LOOKUP
	public Object fulfill(LookupRequest request)
	 {
		String name = request.getName();
		Lookup lookup = MinorityTranslateModel.preferences().getLookups().findLookup(name);
		if (lookup == null) {
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.lookup.error.header"), Messages.getString("messages.lookup.error.lookup.not.found").replaceFirst("#name", name));
			return null;
		}
		
		String search = request.getSearch();
		String url = lookup.getURL();
		String paramaters = lookup.getParameters();
		String srcElement = lookup.getSrcElement();
		String opening = lookup.getOpening();
		String dstElement = lookup.getDstElement();
		String closing = lookup.getClosing();
		
		try {
			search = URLEncoder.encode(search, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		if (url.equals("http://www.dictionaryapi.com/api/v1/references/collegiate/xml/" + SEARCH)) paramaters = paramaters.isEmpty() ? "key=" + MERRIAM_WEBSTER_DICTIONARY_KEY : paramaters + "&" + "key=" + MERRIAM_WEBSTER_DICTIONARY_KEY;
		if (url.equals("http://www.dictionaryapi.com/api/v1/references/thesaurus/xml/" + SEARCH)) paramaters = paramaters.isEmpty() ? "key=" + MERRIAM_WEBSTER_THESAURUS_KEY : paramaters + "&" + "key=" + MERRIAM_WEBSTER_THESAURUS_KEY;
		
		//System.out.println(url);
		
		url = url.replace(SEARCH, search);
		paramaters = paramaters.replace(SEARCH, search);
		
		try {
			
			String response = HTTPUtil.sendGET(url, paramaters);

			//System.out.println(response);
			
			List<String> arguments = ProcessingUtil.collectHTML(srcElement, response);
			
			//System.out.println(arguments);
			
			String replaced = ProcessingUtil.replaceHTML(opening, dstElement, closing, arguments);
			
			return request.fulfill(replaced);
			
		} catch (IOException e) {
			LOGGER.error("Lokup failed!", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.lookup.error.header"), Messages.getString("messages.lookup.error.failed.to.retrieve.content").replaceFirst("#message", e.getClass().getSimpleName() + ": " + e.getMessage()));
			return null;
		}
	 }

	
	
	
	
}
