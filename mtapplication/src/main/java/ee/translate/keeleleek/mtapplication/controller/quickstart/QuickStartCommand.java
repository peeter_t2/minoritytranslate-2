package ee.translate.keeleleek.mtapplication.controller.quickstart;

import java.util.ArrayList;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.dialogs.QuickStartDialogMediator;

public class QuickStartCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		ArrayList<String> pageNames = new ArrayList<>();
		String[] allPages = QuickStartDialogMediator.ALL_PAGES;
		for (int i = 0; i < allPages.length; i++) {
			if (!MinorityTranslateModel.preferences().isQuickStartAckowledged(allPages[i])) pageNames.add(allPages[i]);
		}
		
		if (allPages.length > 0) DialogFactory.dialogs().askQuickStart(pageNames);
	 }

}
