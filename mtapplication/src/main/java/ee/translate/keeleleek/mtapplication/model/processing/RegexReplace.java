package ee.translate.keeleleek.mtapplication.model.processing;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

public class RegexReplace {

	private String rgxFind;
	private String replace = null;

	
	// INIT
	public RegexReplace(String rgxFind, String replace)
	 {
		this.rgxFind = rgxFind;
		this.replace = replace;
	 }
	

	// REPLACE
	public String replace(String text, HashMap<String, String> paramMap)
	 {
		// collected
		String replace = this.replace;
		Set<Entry<String, String>> entries = paramMap.entrySet();
		for (Entry<String, String> entry : entries) {
			rgxFind = rgxFind.replaceAll(Pattern.quote(entry.getKey()), entry.getValue());
			replace = replace.replaceAll(Pattern.quote(entry.getKey()), entry.getValue());
		}
		
		// replace
		return Pattern.compile(rgxFind, Pattern.MULTILINE | Pattern.DOTALL).matcher(text).replaceAll(replace); 
	 }
	
	
}
