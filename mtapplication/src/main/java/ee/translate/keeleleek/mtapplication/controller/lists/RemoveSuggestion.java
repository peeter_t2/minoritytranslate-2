package ee.translate.keeleleek.mtapplication.controller.lists;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.lists.ListSuggestion;

public class RemoveSuggestion extends SimpleCommand {

	
	@Override
	public void execute(INotification notification)
	 {
		ListSuggestion suggestion = (ListSuggestion) notification.getBody();
		
		MinorityTranslateModel.lists().removeSuggestion(suggestion);
	 }
	
}
