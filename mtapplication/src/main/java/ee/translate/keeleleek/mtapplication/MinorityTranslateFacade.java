package ee.translate.keeleleek.mtapplication;

import org.puremvc.java.multicore.patterns.facade.Facade;

import ee.translate.keeleleek.mtapplication.controller.MinorityTranslateController;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.MinorityTranslateView;

public class MinorityTranslateFacade extends Facade {

	final private static String NAME = "{5EE29D1A-584B-4256-87F3-B1AD398D0E67}";

	
	// INIT
	public MinorityTranslateFacade() {
		super(NAME);
	}
	

	@Override
	protected void initializeFacade() {
		initializeModel();
		initializeView();
		initializeController();
	}
	
	@Override
	protected void initializeModel()
	 {
		model = MinorityTranslateModel.getInstance(multitonKey);
	 }

	@Override
	protected void initializeView()
	 {
		view = MinorityTranslateView.getInstance(multitonKey);
	 }

	@Override
	protected void initializeController()
	 {
		controller = MinorityTranslateController.getInstance(multitonKey);
	 }
	
	

	// LAUNCH
	/**
	 * Launches the application,
	 * 
	 * @param translateWindow translate window
	 */
	public void launch(Object translateWindow)
	 {
		// preferences
		sendNotification(Notifications.PREFERENCES_LOAD);

		// wikis
		sendNotification(Notifications.FETCH_WIKIS);

		// lists
		sendNotification(Notifications.FETCH_LISTS);
		
		// prepare
		sendNotification(Notifications.PREPARE_GUI, translateWindow);

		// initial session
		sendNotification(Notifications.SESSION_NEW);

		// connection
		sendNotification(Notifications.CONNECTION_MONITORING_START);

		// timer
		sendNotification(Notifications.TIMER_START);

		// log in
		sendNotification(Notifications.LOGIN);
		
		// show
		sendNotification(Notifications.WINDOW_TRANSLATE_OPEN);
	 }
	
	public void done()
	 {
		// quick-start
		sendNotification(Notifications.SHOW_QUICK_START);
	 }
	
	
}
