package ee.translate.keeleleek.mtapplication.view.dialogs;

import java.nio.file.Path;
import java.util.List;

import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.DialogFactoryFX;
import ee.translate.keeleleek.mtapplication.view.javafx.dialogs.DialogFactoryFXBackwardCompatible;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;

public abstract class DialogFactory {

	private static DialogFactory dialogs;
	
	public static DialogFactory dialogs()
	 {
		if (dialogs != null) return dialogs;
		
		try {
			dialogs = new DialogFactoryFX();
		} catch (Throwable e) {
			dialogs = new DialogFactoryFXBackwardCompatible();
		}
		
		return dialogs;
	 }
	
	
	// INHERIT
	protected abstract String askString(String title, String header, String content, String initial, boolean allowEmpty);
	protected abstract boolean askConfirmDecline(String title, String header, String content, String confirm, String cancel);
	protected abstract YesNoCancel askConfirmYesNoCancel(String title, String header, String content, String yes, String no, String cancel);
	
	public abstract void showError(String header, String content);
	public abstract void showWarning(String header, String content);
	public abstract void showInfo(String header, String content);
	
	public abstract PullCallback showPullProgressDialog(String title, String header, String ok, String cancel);
	public abstract PullCallback showPullMappingsDialog(String title, String header, String ok, String cancel);
	
	
	// ASKING
	public String askTag()
	 {
		return askString(Messages.getString("tag.title"), Messages.getString("tag.header"), Messages.getString("tag.content"), MinorityTranslateModel.session().getTag(), true);
	 }
	
	public abstract void askQuickStart(List<String> allPages);

	public abstract String askUnicodeCharacter();

	public abstract Path askSymbolsSavePath();

	public abstract Path askSymbolsOpenPath();

	public abstract List<ContentRequest> askAddContent();
	
	public boolean confirmReset()
	 {
		return askConfirmDecline(Messages.getString("messages.reset.article.confirmation.title"), Messages.getString("messages.reset.article.confirmation.header"), Messages.getString("messages.reset.article.confirmation.content"), Messages.getString("confirmation.reset"), Messages.getString("button.cancel"));
	 }

	public YesNoCancel confirmSaveExit()
	 {
		return askConfirmYesNoCancel(Messages.getString("confirmation.save.title"), Messages.getString("confirmation.save.quit.message"), Messages.getString("confirmation.save.details"), Messages.getString("confirmation.save.yes"), Messages.getString("confirmation.save.no"), Messages.getString("button.cancel"));
	 }

	public YesNoCancel confirmNewSession()
	 {
		return askConfirmYesNoCancel(Messages.getString("confirmation.save.title"), Messages.getString("confirmation.save.new.session.message"), Messages.getString("confirmation.save.details"), Messages.getString("confirmation.save.yes"), Messages.getString("confirmation.save.no"), Messages.getString("button.cancel"));
	 }
	

	public enum YesNoCancel
	 {
		YES,
		NO,
		CANCEL
	 }

	public enum ConfirmDecline
	 {
		CONFIRM,
		DECLINE,
		UNKNOWN
	 }
	
}
