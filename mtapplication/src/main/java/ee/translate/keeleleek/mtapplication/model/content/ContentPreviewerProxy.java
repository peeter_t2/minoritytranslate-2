package ee.translate.keeleleek.mtapplication.model.content;

import java.util.ArrayList;
import java.util.Queue;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.FetchPreviewAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class ContentPreviewerProxy extends Proxy implements Runnable {

	public final static String NAME = "{1BCE94FC-0761-4441-AFE0-02B83D674E17}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(ContentPreviewerProxy.class);
	
	public final static long TIMEOUT = 250*0 + 1000;
	public final static int MAX_DOWNLOADS = 10;
	
	private ArrayList<FullRequest> normalFetching = new ArrayList<>();
	private ArrayList<FullRequest> filterFetching = new ArrayList<>();
	
	private boolean busy = false;
	private boolean run = false;

	
	// INIT
	public ContentPreviewerProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister() {
		run = true;
		
		startThread();
	}
	
	@Override
	public void onRemove() {
		run = false;
	}

	private void startThread()
	 {
		Thread thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	 }
	
	
	// WORK
	@Override
	public void run()
	 {
		try {
			while (run) {
				work();
				Thread.sleep(TIMEOUT);
			}
		}
		catch (InterruptedException e) {
			LOGGER.error("Thread sleep failure!", e);
		}
		catch (IllegalStateException e) {
			LOGGER.warn("Failed to preview content: " + e.getMessage() + "!");
			startThread();
		}
	 }

	private void work()
	 {
		lookBusy();
		
		workNormal();

		workFiltered();
		
		lookBusy();
	 }
	
	private void workNormal()
	 {
		if (normalFetching.size() >= MAX_DOWNLOADS) return;
		
		Queue<MinorityArticle> articles = ContentUtil.filterPreviewArticles(MinorityTranslateModel.content());
		while (!articles.isEmpty() && normalFetching.size() < MAX_DOWNLOADS) {
			
			MinorityArticle article = articles.remove();
			FullRequest fullRef = article.createFullRef();
			if (normalFetching.contains(fullRef)) continue;
			fetch(fullRef);
        
		}
	 }

	private void workFiltered()
	 {
		if (filterFetching.size() >= MAX_DOWNLOADS) return;
		
		Queue<MinorityArticle> articles = ContentUtil.filterFilteredPreviewArticles(MinorityTranslateModel.content());
		while (!articles.isEmpty() && filterFetching.size() < MAX_DOWNLOADS) {
			
			MinorityArticle article = articles.remove();
			FullRequest fullRef = article.createFullRef();
			if (filterFetching.contains(fullRef)) continue;
			filterFetch(fullRef);
        
		}
	 }

	private void lookBusy()
	 {
		boolean busy = normalFetching.size() > 0 || filterFetching.size() > 0;
		
		if (this.busy == busy) return;
		
		this.busy = busy;
		
		MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_PREVIEWER_BUSY, busy);
	 }
	
	
	// DOWNLOADING
	private void fetch(final FullRequest fullRef)
	 {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run()
			 {
				Reference ref = fullRef.createReference();
				
				normalFetching.add(fullRef);
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_PREVIEW_FETCHING_STARTED, ref);
				
				String langCode = fullRef.getLangCode();

				String text = MinorityTranslateModel.content().getText(ref);
				String preview = "";
				if (text != null) {
					
					MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
					FetchPreviewAction action = new FetchPreviewAction(text);
					bot.getPerformedAction(action);
					
					preview = action.getParsedText();
					
				}
				
				MinorityTranslateModel.content().changePreview(ref, text, preview);

				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_PREVIEW_FETCHING_ENDED, ref);
				normalFetching.remove(fullRef);
				
				LOGGER.info("Preview fetched for article with reference " + ref);
			 }
		});
		
		LOGGER.info("Fetching preview for " + fullRef);
		thread.start();
		
	 }
	
	private void filterFetch(final FullRequest fullRef)
	 {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run()
			 {
				Reference ref = fullRef.createReference();
				
				filterFetching.add(fullRef);
				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_PREVIEW_FETCHING_STARTED, ref);
				
				String langCode = fullRef.getLangCode();

				String text = MinorityTranslateModel.content().getFilteredText(ref);
				String preview = "";
				if (text != null) {
					
					MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
					FetchPreviewAction action = new FetchPreviewAction(text);
					bot.getPerformedAction(action);
					
					preview = action.getParsedText();
					
				}
				
				MinorityTranslateModel.content().changeFilteredPreview(ref, text, preview);

				MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.ENGINE_PREVIEW_FETCHING_ENDED, ref);
				filterFetching.remove(fullRef);
				
				LOGGER.info("Filtered preview fetched for article with reference " + ref);
			 }
		});
		
		LOGGER.info("Fetching filtered preview for " + fullRef);
		thread.start();
		
	 }
	
	
}
