package ee.translate.keeleleek.mtapplication.controller.lists;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.MinorityTranslate;
import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.lists.ListsProxy;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class FetchListsCommand extends SimpleCommand {

	public final static Path LISTS_PATH = FileSystems.getDefault().getPath(MinorityTranslate.ROOT_PATH, "lists.json");
	public final static String LISTS_JAR_PATH = "/lists.json";
	
	private static Logger LOGGER = LoggerFactory.getLogger(FetchListsCommand.class);

	
	@Override
	public void execute(INotification notification)
	 {
		LOGGER.info("Reading lists");
		
		String json;
		try {
			
			if (LISTS_PATH.toFile().isFile()) {
				json = FileUtil.read(LISTS_PATH);
			} else {
				json = FileUtil.readFromJar(LISTS_JAR_PATH);
			}
			
		} catch (IOException e) {
			LOGGER.error("Failed to read lists", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.lists.load.failed"), Messages.getString("messages.lists.load.failed.to").replaceFirst("#", LISTS_PATH.toString()).replaceFirst("#",e.getMessage()));
			return;
		}
		
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
		builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
		Gson gson = builder.create();
		
		ListsProxy listsProxy = gson.fromJson(json, ListsProxy.class);

		getFacade().registerProxy(listsProxy);
	 }
	
}
