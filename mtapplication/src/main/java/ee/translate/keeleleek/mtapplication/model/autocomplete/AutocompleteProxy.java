package ee.translate.keeleleek.mtapplication.model.autocomplete;

import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.puremvc.java.multicore.patterns.proxy.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.controller.actions.SearchAction;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.bots.BotsProxy;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import net.sourceforge.jwbf.mediawiki.MediaWiki;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class AutocompleteProxy extends Proxy {

	public final static String NAME = "{5E079738-1F7C-4EFC-B24E-D4436CD2884B}";
	
	private static Logger LOGGER = LoggerFactory.getLogger(AutocompleteProxy.class);
	
//	private static AutocompleteChoice[] SYNTAX_CHOISES = new AutocompleteChoice[]
//	 {
//		new AutocompleteChoice("[[]]","[[]]"),
//		new AutocompleteChoice("{{}}","{{}}"),
//		new AutocompleteChoice("<ref></ref>","<ref></ref>"),
//		new AutocompleteChoice("[[File:|thumb|]]","[[File:|thumb|]]"),
//		new AutocompleteChoice("<gallery></gallery>","<gallery>\n\n</gallery>"),
//		new AutocompleteChoice("<small></small>","<small></small>"),
//		new AutocompleteChoice("<nowiki></nowiki>","<nowiki></nowiki>")
//	 };
	
	
	// INIT
	public AutocompleteProxy() {
		super(NAME, "");
	}

	@Override
	public void onRegister()
	 {
		
	 }
	
	@Override
	public void onRemove() {
		
	}
	
	
	// PROCESSING
	public void request(final AutocompleteRequest request) throws PatternSyntaxException
	 {
		LOGGER.info("Requesting autocomplete");
		
		// crop
		String prefix = request.getPreceeding();
		
		int i = -1;
		
		int iOpenRect = prefix.lastIndexOf("[[");
		int iCloseRect = prefix.lastIndexOf("]]");
		int iOpenSquig = prefix.lastIndexOf("{{");
		int iCloseSquig = prefix.lastIndexOf("}}");
		int iOpenAng = prefix.lastIndexOf("<");
		int iCloseAng = prefix.lastIndexOf(">");
		
		if (iOpenRect != -1 && iOpenRect > iCloseRect) i = iOpenRect; // open rectangular brackets
		else if (iOpenSquig != -1 && iOpenSquig > iCloseSquig) i = iOpenSquig; // open squigly brackets
		else if (iOpenAng != -1 && iOpenAng > iCloseAng) i = iOpenAng; // open ang brackets
		else {
			i = prefix.lastIndexOf('\n');
			int j = prefix.lastIndexOf(' ');
			if (j > i) i = j;
			if (iCloseRect > i) i = iCloseRect;
			if (iCloseSquig > i) i = iCloseSquig;
			i++;
		}
		
		if (i != -1) prefix = prefix.substring(i);
		
		// collect choices
		final AutocompleteChoices allChoices = new AutocompleteChoices(request.getRef());
		List<AutocompleteChoice> choises = MinorityTranslateModel.preferences().findAutocompletes(prefix, request.getRef().getLangCode());
		allChoices.addChoises(choises);
		
		// search choices
		if (MinorityTranslateModel.connection().hasConnection() && MinorityTranslateModel.preferences().isSearchLinks() && isSearch(prefix)) {
			
			final String search = prefix;
			
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {

					AutocompleteChoice[] searchChoises = searchChoises(request.getRef(), search);
					if (searchChoises != null) allChoices.addChoises(searchChoises);

					MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SHOW_AUTOCOMPLETE, allChoices);
					
				}
			});
			thread.start();
			
		}
		
		// templates
		else {

			MinorityTranslateModel.notifier().sendNotificationThreadSafe(Notifications.SHOW_AUTOCOMPLETE, allChoices);
			
		}

	 }

	private AutocompleteChoice[] searchChoises(Reference ref, String prefix)
	 {
		// type
		String type = findType(prefix);
		String search = findSearch(prefix);
		
		if (search.isEmpty()) return new AutocompleteChoice[0];
		
		boolean lower = search.charAt(0) == Character.toLowerCase(search.charAt(0));
		
		String[][] searches;
		switch (type.toLowerCase()) {
		case "files":
			searches = fetchSearch(BotsProxy.COMMONS_LANG_CODE, type + ":" + search);
			break;

		default:
			searches = fetchSearch(ref.getLangCode(), type + ":" + search);
			break;
		}
		
		AutocompleteChoice[] choises = new AutocompleteChoice[searches.length];
		for (int k = 0; k < choises.length; k++) {
			
			String name = searches[k][0];
			String paste = searches[k][0];
			String description = searches[k][1];
			
			int j = name.indexOf(':');
			if (j != -1) name = name.substring(j + 1);

			j = paste.indexOf(':');
			if (j != -1) paste = paste.substring(j + 1);
			
			if (lower) {
				if (!name.isEmpty()) name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
				if (!paste.isEmpty()) paste = Character.toLowerCase(paste.charAt(0)) + paste.substring(1);
			}
			
			choises[k] = new AutocompleteChoice(name, paste, description, search.length(), 0);
			
		}
		
		return choises;
	 }
	

	// HELPERS
	private String[][] fetchSearch(String langCode, String search)
	 {
		SearchAction action = new SearchAction(search, MediaWiki.NS_MAIN);
		MediaWikiBot bot = MinorityTranslateModel.bots().fetchBot(langCode);
		bot.getPerformedAction(action);
		return action.getExtendedResults();
	 }
	
	private boolean isSearch(String prefix)
	 {
		String type = findType(prefix);
		String search = findSearch(prefix);
		
		if (!type.isEmpty() && !search.isEmpty()) return true;
		if (prefix.startsWith("[[") && type.isEmpty() && !search.isEmpty()) return true;
		
		return false;
	 }
	
	private String findType(String prefix)
	 {
		if (prefix.startsWith("[[")) prefix = prefix.substring(2);
		
		String type = "";
		int i = prefix.indexOf(':');
		if (i != -1)  type = prefix.substring(0, i);
		type = type.trim();
		
		return type;
	 }

	private String findSearch(String prefix)
	 {
		if (prefix.startsWith("[[")) prefix = prefix.substring(2);
		
		String search = prefix;
		int i = prefix.indexOf(':');
		if (i != -1)  search = prefix.substring(i + 1);
		search.trim();
		
		return search;
	 }
	
	
}
