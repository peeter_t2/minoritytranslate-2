package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.ConfirmationDialogMediator;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class ConfirmationDialogMediatorFX extends ConfirmationDialogMediator {

	final public static String NAME = "{D2386EEB-B1F7-4D2F-B18A-9AEAC249E128}";

	@FXML
	private Label messageLabel;
	@FXML
	private Label detailsLabel;

	@FXML
	private Button yesButton;
	@FXML
	private Button noButton;
	@FXML
	private Button cancelButton;
	
	
	// INIT:
	public ConfirmationDialogMediatorFX() {
		super();
	}
	
	
	// IMPLEMENTATION:
	@Override
	public void close() {
		((Stage)((Parent)getViewComponent()).getScene().getWindow()).close();
	}
	
	@Override
	public void setText(String message, String details, String yes, String no, String cancel) {
		messageLabel.setText(message);
		detailsLabel.setText(details);
		yesButton.setText(yes);
		noButton.setText(no);
		cancelButton.setText(cancel);
	}
	
	
	// BUTTONS:
	@FXML
	@Override
	public void onYesClick() {
		super.onYesClick();
    }

	@FXML
	@Override
	public void onNoClick() {
		super.onNoClick();
    }

	@FXML
	@Override
	public void onCancelClick() {
		super.onCancelClick();
    }

    
}
