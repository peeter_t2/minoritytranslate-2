package ee.translate.keeleleek.mtapplication.controller.populating;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;

public class RemoveArticleCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String title = (String) notification.getBody();
		String langCode = notification.getType();
		String qid = MinorityTranslateModel.content().findQid(langCode, title);
		
		if (qid != null) MinorityTranslateModel.content().removeQid(qid);
	 }
	
}
