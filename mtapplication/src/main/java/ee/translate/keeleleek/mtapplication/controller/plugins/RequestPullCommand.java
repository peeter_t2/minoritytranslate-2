package ee.translate.keeleleek.mtapplication.controller.plugins;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.translate.keeleleek.mtapplication.common.requests.PullRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;
import ee.translate.keeleleek.mtpluginframework.pull.PullPlugin;
import ee.translate.keeleleek.mtpluginframework.pull.UnsupportedLanguagePairException;

public class RequestPullCommand extends SimpleCommand {

	private static Logger LOGGER = LoggerFactory.getLogger(RequestPullCommand.class);
	
	@Override
	public void execute(INotification notification)
	 {
		final PullRequest req = (PullRequest) notification.getBody();
		
		final PullCallback progress = MinorityTranslateModel.preferences().isShowAssociationDialog()
				? DialogFactory.dialogs().showPullMappingsDialog(Messages.getString("plugins.pull.title"), Messages.getString("plugins.pull.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"))
				: DialogFactory.dialogs().showPullProgressDialog(Messages.getString("plugins.pull.title"), Messages.getString("plugins.pull.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					
					MinorityTranslateModel.pull().requestPull(req, progress);
					
				} catch (final UnsupportedLanguagePairException e) {
					MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
						@Override
						public void run() {
							
							progress.finish(false);
							
							PullPlugin plugin = MinorityTranslateModel.pull().getPlugin(e.getPluginName());
							String pluginName = null;
							if (plugin != null) pluginName = plugin.getName(MinorityTranslateModel.preferences().getGUILangCode());
							if (pluginName == null) pluginName = e.getPluginName();
							
							String content = Messages.getString("messages.pull.failed.content.language.pair.not.supported");
							content = content.replace("#", pluginName);
							content = content.replace("#", e.getSrcLangCode());
							content = content.replace("#", e.getDstLangCode());
							
							DialogFactory.dialogs().showError(Messages.getString("messages.pull.failed.header"), content);
							
						}
					});
				} catch (final Exception e) {
					MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
						@Override
						public void run() {
							
							LOGGER.error("Pull failed", e);
							progress.finish(false);
							DialogFactory.dialogs().showError(Messages.getString("messages.pull.failed.header"), Messages.getString("messages.pull.failed.content.exception").replace("#", e.toString()));
							
						}
					});
				}
			}
		});
		thread.start();
	 }
	
}
