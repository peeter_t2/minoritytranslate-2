package ee.translate.keeleleek.mtapplication.model.processing.processers;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.PatternSyntaxException;

import ee.translate.keeleleek.mtapplication.model.processing.RegexCollect;

public class RegexCollectProcesser {

	final public static String SRC_TITLE_KEY = "${Src_title}";
	final public static String SRC_TITLE_KEY_LOWER_CASE = "${src_title}";
	
	
	public String process(String text, String articleTitle, String articleText, List<RegexCollect> regexCollects) throws PatternSyntaxException
	 {
		HashMap<String, String> paramMap = new HashMap<>();

		if (!articleTitle.isEmpty()) {
			paramMap.put(SRC_TITLE_KEY, articleTitle);
			paramMap.put(SRC_TITLE_KEY_LOWER_CASE, decapitalise(articleTitle));
		}
		
		for (RegexCollect regexCollect : regexCollects) {
			regexCollect.collect(articleText, paramMap);
		}
		
		Set<Entry<String, String>> entries = paramMap.entrySet();
		for (Entry<String, String> entry : entries) {
			text = text.replace(entry.getKey(), entry.getValue());
		}
		
		return text;
	 }
	
	
	public String capitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public String decapitalise(String str){
	    if(str.length() == 0) return str;
	    return str.substring(0, 1).toLowerCase() + str.substring(1);
	}
	
}
