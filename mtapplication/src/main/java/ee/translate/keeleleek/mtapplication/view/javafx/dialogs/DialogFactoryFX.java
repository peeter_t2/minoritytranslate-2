package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.requests.ContentRequest;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.view.dialogs.ConfirmDeclineDialog;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.dialogs.PullProgressDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.QuickStartDialogMediator;
import ee.translate.keeleleek.mtapplication.view.dialogs.UnicodeTableDialogMediator;
import ee.translate.keeleleek.mtapplication.view.javafx.MediatorLoaderFX;
import ee.translate.keeleleek.mtapplication.view.javafx.fxemelents.ExposableConfirmFX;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;
import ee.translate.keeleleek.mtpluginframework.mapping.PullMapping;
import ee.translate.keeleleek.mtpluginframework.pull.PullCallback;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;

public class DialogFactoryFX extends DialogFactory implements ApplicationDialogFactoryFX {

	private final static String SYMBOLS_EXTENSION = "symbols";
	
	private static Window owner = null;
	
	private static FileChooser symbolSetChooser = null;

	private static FileChooser symbolSetChooser()
	{
		if (symbolSetChooser != null) return symbolSetChooser;
		symbolSetChooser = new FileChooser();
		ExtensionFilter extFilter = new ExtensionFilter(Messages.getString("preferences.content.assist.symbols.file.name"), "*." + SYMBOLS_EXTENSION);
		symbolSetChooser.getExtensionFilters().add(extFilter);
		return symbolSetChooser;
	}
	
	public static void setup(Stage transtaleWindow) {
		owner = transtaleWindow;
	}
	
	@Override
	public String askString(String title, String header, String content, String initial, boolean allowEmpty)
	 {
		// create
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 60, 10, 10));

		final TextField textField = new TextField(initial);
		
		grid.add(new Label(content), 0, 0);
		grid.add(textField, 1, 0);
		
		dialog.getDialogPane().setContent(grid);
		
		final ButtonType okButtonType = new ButtonType(Messages.getString("button.ok"), ButtonData.OK_DONE);
		ButtonType cancelButtonType = new ButtonType(Messages.getString("button.cancel"), ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(okButtonType, cancelButtonType);
		
		// disable
		if (!allowEmpty) {
			final Node okButton = dialog.getDialogPane().lookupButton(okButtonType);
			okButton.setDisable(textField.getText().trim().isEmpty());
			
			textField.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					okButton.setDisable(newValue.trim().isEmpty());
				}
			});
		}
		
		// result convert
		dialog.setResultConverter(new Callback<ButtonType, String>() {
			@Override
			public String call(ButtonType button) {
				if (button != okButtonType) return null;
				return textField.getText().trim();
			}
		});
		
		// focus
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				textField.requestFocus();
			}
		});
		
		Optional<String> result = dialog.showAndWait();
		
		if (result.isPresent()) return result.get();
		else return null;
	 }
	
	@Override
	protected boolean askConfirmDecline(String title, String header, String content, String yes, String cancel)
	 {
		// create
		Dialog<Boolean> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 60, 10, 10));

		grid.add(new Label(content), 0, 0);
		
		dialog.getDialogPane().setContent(grid);
		
		final ButtonType yesButtonType = new ButtonType(yes, ButtonData.OK_DONE);
		ButtonType cancelButtonType = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(yesButtonType, cancelButtonType);
		
		// result convert
		dialog.setResultConverter(new Callback<ButtonType, Boolean>() {
			@Override
			public Boolean call(ButtonType button) {
				if (button != yesButtonType) return false;
				return true;
			}
		});
		
		Optional<Boolean> result = dialog.showAndWait();
		
		if (result.isPresent()) return result.get();
		else return false;
	 }
	
	@Override
	protected YesNoCancel askConfirmYesNoCancel(String title, String header, String content, String yes, String no, String cancel)
	 {
		// create
		Dialog<YesNoCancel> dialog = new Dialog<>();
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 60, 10, 10));

		grid.add(new Label(content), 0, 0);
		
		dialog.getDialogPane().setContent(grid);
		
		final ButtonType yesButtonType = new ButtonType(yes, ButtonData.YES);
		final ButtonType noButtonType = new ButtonType(no, ButtonData.NO);
		ButtonType cancelButtonType = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(yesButtonType, noButtonType, cancelButtonType);
		
		// result convert
		dialog.setResultConverter(new Callback<ButtonType, YesNoCancel>() {
			@Override
			public YesNoCancel call(ButtonType button) {
				if (button == yesButtonType) return YesNoCancel.YES;
				if (button == noButtonType) return YesNoCancel.NO;
				return YesNoCancel.CANCEL;
			}
		});
		
		Optional<YesNoCancel> result = dialog.showAndWait();
		
		if (result.isPresent()) return result.get();
		else return YesNoCancel.CANCEL;
	 }
	
	@Override
	public void showError(String header, String content)
	 {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(Messages.getString("messages.error"));
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.setGraphic(null);
		if (owner != null) alert.initOwner(owner);

		alert.showAndWait();
	 }
	
	@Override
	public void showWarning(String header, String content)
	 {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(Messages.getString("messages.warning"));
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.setGraphic(null);
		if (owner != null) alert.initOwner(owner);

		alert.showAndWait();
	 }
	
	@Override
	public void showInfo(String header, String content)
	 {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(Messages.getString("messages.info"));
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.setGraphic(null);
		if (owner != null) alert.initOwner(owner);

		alert.showAndWait();
	 }
	
	@Override
	public PullCallback showPullProgressDialog(String title, String header, String ok, String cancel)
	 {
		final Dialog<?> dialog = new Dialog<>();
		dialog.setResizable(true);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		final PullProgressDialogMediator mediator = (PullProgressDialogMediator) MediatorLoaderFX.loadPullProgressMediator();
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		ButtonType cancelButtonType = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(cancelButtonType);
		
		final PullCallback progress = new PullCallback() {
			@Override
			protected void onStart(final int total) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setTotal(total);
						mediator.setCurrent(0);
					}
				});
				
			}

			@Override
			protected void onAction(final String action) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setAction(action);
					}
				});
			}

			@Override
			protected void onAdvance(final int current) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setCurrent(current);
					}
				});
			}

			@Override
			protected void onMap(PullMapping map) { }
			
			@Override
			protected void onEnd() {
				
			}
			
			@Override
			protected void onFinish(boolean success) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						dialog.close();
					}
				});
			}
		};
		progress.confirm();

		final Button cancelButton = (Button) dialog.getDialogPane().lookupButton(cancelButtonType);
		
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				progress.decline();
			}
		});
		
		dialog.setOnCloseRequest(new EventHandler<DialogEvent>() {
			@Override
			public void handle(DialogEvent event) {
				if (!progress.isResolved()) progress.decline();
			}
		});
		
		dialog.show();
		
		return progress;
	 }
	
	@Override
	public PullCallback showPullMappingsDialog(String title, String header, String ok, String cancel)
	 {
		// create
		Dialog<?> dialog = new Dialog<>();
		dialog.setResizable(true);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		final ButtonType okButtonType = new ButtonType(ok, ButtonData.OK_DONE);
		ButtonType cancelButtonType = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(okButtonType, cancelButtonType);
		
		final Button cancelButton = (Button) dialog.getDialogPane().lookupButton(cancelButtonType);
		final Button okButton = (Button) dialog.getDialogPane().lookupButton(okButtonType);
		okButton.setDisable(true);
		
		final PullMappingsDialogMediatorFX mediator = (PullMappingsDialogMediatorFX) MediatorLoaderFX.loadPullMappingsDialogMediator();
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		// prepare and focus
		final ConfirmDeclineDialog confirmDeclineDialog = mediator instanceof ConfirmDeclineDialog ? (ConfirmDeclineDialog) mediator : null;

		if (confirmDeclineDialog != null) confirmDeclineDialog.prepare();
		
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (confirmDeclineDialog != null) confirmDeclineDialog.focus();
			}
		});
		
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		// progress
		final PullCallback progress = new PullCallback() {
			@Override
			protected void onStart(final int total) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setTotal(total);
						mediator.setCurrent(0);
						
					}
				});
				
			}

			@Override
			protected void onAction(final String action) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setAction(action);
					}
				});
			}
			
			@Override
			protected void onAdvance(final int current) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						mediator.setCurrent(current);
					}
				});
			}

			@Override
			protected void onMap(PullMapping mapping) {
				mediator.addMapping(mapping);
			}
			
			@Override
			protected void onEnd() {
				
			}
			
			@Override
			protected void onFinish(boolean success) {
				MinorityTranslateModel.notifier().doThreadSafe(new Runnable() {
					@Override
					public void run() {
						okButton.setDisable(false);
						mediator.setDone();
					}
				});
			}
		};

		// conclusions
		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				progress.confirm();
				if (mediator.isSaveTemplateMappings()) progress.saveTemplateMappings();
				if (confirmDeclineDialog != null) confirmDeclineDialog.confirm();
			}
		});

		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				progress.decline();
				if (confirmDeclineDialog != null) confirmDeclineDialog.decline();
			}
		});
		
		dialog.setOnCloseRequest(new EventHandler<DialogEvent>() {
			@Override
			public void handle(DialogEvent event) {
				if (!progress.isResolved()) progress.decline();
				if (confirmDeclineDialog != null) confirmDeclineDialog.decline();
			}
		});
		
		// show!
		dialog.show();
		
		return progress;
	 }
	
	@Override
	public void askQuickStart(List<String> allPages)
	 {
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle(Messages.getString("quick.start.title"));
		
		if (owner != null) dialog.initOwner(owner);
		
		ButtonType nextButtonType = new ButtonType(Messages.getString("quick.start.next.button"), ButtonData.NEXT_FORWARD);
		dialog.getDialogPane().getButtonTypes().addAll(nextButtonType);
		
		final QuickStartDialogMediator mediator = MediatorLoaderFX.loadQuickStartDialogMediator();
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		mediator.open(allPages);
		
		final Button button = (Button) dialog.getDialogPane().lookupButton(nextButtonType);
		if (mediator.isLast()) button.setText(Messages.getString("quick.start.finish.button"));

		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event)
			 {
				String pageName = mediator.onClosePage();
				MinorityTranslateModel.notifier().sendNotification(Notifications.PREFERENCES_ACKNOWLEDGE, pageName);
				if (mediator.isLast()) button.setText(Messages.getString("quick.start.finish.button"));
			 }
		});
		
		dialog.setOnCloseRequest(new EventHandler<DialogEvent>() {
			@Override
			public void handle(DialogEvent event)
			 {
				if (!mediator.isLast()) event.consume();
			 }
		});
		
		if (mediator.hasPages()) dialog.showAndWait();
	 }

	@Override
	public String askUnicodeCharacter()
	 {
		UnicodeTableDialogMediator mediator = MediatorLoaderFX.loadUnicodeDialogMediator();
		
		ConfirmDecline result = showConfirmDeclineDialog(mediator, Messages.getString("unicode.table.title"), null, Messages.getString("button.add"), Messages.getString("button.cancel"));
		
		if (result == ConfirmDecline.CONFIRM) {
			return mediator.getSymbols();
		}
		
		return null;
	 }
	
	@Override
	public Path askSymbolsOpenPath()
	 {
		FileChooser symbolSetChooser = symbolSetChooser();
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		symbolSetChooser.setInitialDirectory(initial.toFile());
		File file = symbolSetChooser.showOpenDialog(owner);
		if (file == null) return null;
		return file.toPath();
	 }

	@Override
	public Path askSymbolsSavePath()
	 {
		FileChooser symbolSetChooser = symbolSetChooser();
		Path initial = MinorityTranslateModel.session().getWorkingPath();
		symbolSetChooser.setInitialDirectory(initial.toFile());
		File file = symbolSetChooser.showSaveDialog(owner);
		if (file == null) return null;
		if(!file.getName().endsWith("." + SYMBOLS_EXTENSION)) file = new File(file.getAbsolutePath() + "." + SYMBOLS_EXTENSION);
		return file.toPath();
	 }
	
	@Override
	public List<ContentRequest> askAddContent()
	 {
		AddContentDialogMediatorFX mediator = (AddContentDialogMediatorFX) MediatorLoaderFX.loadAddContentDialogMediator();
		
		ConfirmDecline result = showConfirmDeclineDialog(mediator, Messages.getString("add.content.dialog.title"), Messages.getString("add.content.dialog.header"), Messages.getString("button.ok"), Messages.getString("button.cancel"));
		
		if (result == ConfirmDecline.CONFIRM) {
			return mediator.collectContentRequests();
		}
		
		return new ArrayList<>();
	 }
	
	@Override
	public ConfirmDecline showConfirmDeclineDialog(Mediator mediator, String title, String header, String ok, String cancel)
	 {
		Dialog<ConfirmDecline> dialog = new Dialog<>();
		dialog.setResizable(true);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		final ButtonType confirmButtonType = new ButtonType(ok, ButtonData.OK_DONE);
		final ButtonType declineButtonType = new ButtonType(cancel, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, declineButtonType);
		
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		// result convert
		dialog.setResultConverter(new Callback<ButtonType, ConfirmDecline>() {
			@Override
			public ConfirmDecline call(ButtonType button) {
				if (button == confirmButtonType) return ConfirmDecline.CONFIRM;
				if (button == declineButtonType) return ConfirmDecline.DECLINE;
				return ConfirmDecline.UNKNOWN;
			}
		});
		
		if (mediator instanceof ExposableConfirmFX) {
			final Button button = (Button) dialog.getDialogPane().lookupButton(confirmButtonType);
			((ExposableConfirmFX) mediator).exposeConfirm(button);
		}
		
		final ConfirmDeclineDialog confirmDeclineDialog = mediator instanceof ConfirmDeclineDialog ? (ConfirmDeclineDialog) mediator : null;

		if (confirmDeclineDialog != null) confirmDeclineDialog.prepare();
		
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (confirmDeclineDialog != null) confirmDeclineDialog.focus();
			}
		});
		
		Optional<ConfirmDecline> result = dialog.showAndWait();
		
		if (confirmDeclineDialog != null) {
			if (result.isPresent() && result.get() == ConfirmDecline.CONFIRM) confirmDeclineDialog.confirm();
			else confirmDeclineDialog.decline();
		}
		
		return result.get();
	 }
	
	@Override
	public void showCloseDialog(Mediator mediator, String title, String header, String close)
	 {
		Dialog<?> dialog = new Dialog<>();
		dialog.setResizable(true);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		
		if (owner != null) dialog.initOwner(owner);
		
		ButtonType okButtonType = new ButtonType(close, ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(okButtonType);
		
		dialog.getDialogPane().setContent((Node) mediator.getViewComponent());
		
		dialog.showAndWait();
	 }
	
}
