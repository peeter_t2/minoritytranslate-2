package ee.translate.keeleleek.mtapplication.view.javafx.dialogs;

import ee.translate.keeleleek.mtapplication.view.dialogs.MessageDialogMediator;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class MessageDialogMediatorFX extends MessageDialogMediator {
	
	@FXML
	private Label messageLabel;
	
	@FXML
	private Label detailsLabel;

	
	// IMPLEMENTATION:
	@Override
	public void setText(String message, String details) {
		messageLabel.setText(message);
		detailsLabel.setText(details);
	}
	
	@Override
	public void close() {
		((Stage)((Parent)getViewComponent()).getScene().getWindow()).close();
	}

	
	// BUTTONS:
	@FXML
	@Override
    public void onOkClick() {
		super.onOkClick();
    }
    
}
