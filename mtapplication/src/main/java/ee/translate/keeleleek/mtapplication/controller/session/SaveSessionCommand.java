package ee.translate.keeleleek.mtapplication.controller.session;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.common.FileUtil;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.model.content.WikiReference;
import ee.translate.keeleleek.mtapplication.model.serialising.ReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.serialising.WikiReferenceSerializer;
import ee.translate.keeleleek.mtapplication.model.session.SessionProxy;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class SaveSessionCommand extends SimpleCommand {

	public final static Charset ENCODING = StandardCharsets.UTF_8;
	
	private Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	
	@Override
	public void execute(INotification notification)
	 {
		Path path = (Path) notification.getBody();
		
		sendNotification(Notifications.SESSION_SAVING_BUSY, true);
		
		MinorityTranslateModel.content().setSaved();
		
		SessionProxy sessionProxy = MinorityTranslateModel.session();
		
		try {
			
			GsonBuilder builder = new GsonBuilder();
			builder.registerTypeAdapter(Reference.class, new ReferenceSerializer());
			builder.registerTypeAdapter(WikiReference.class, new WikiReferenceSerializer());
			builder.setPrettyPrinting();
			Gson gson = builder.create();
			String json = gson.toJson(sessionProxy);
			FileUtil.write(path, json);

			sessionProxy.setSessionPath(path);
			
			getFacade().sendNotification(Notifications.SESSION_SAVED, path);
			
		} catch (IOException e) {
			LOGGER.error("Failed to write session", e);
			sendNotification(Notifications.ERROR_MESSAGE, Messages.getString("messages.session.save.failed"), Messages.getString("messages.session.save.failed.to").replaceFirst("#", path.toString()).replaceFirst("#",e.getMessage()));
		} finally {
			sendNotification(Notifications.SESSION_SAVING_BUSY, false);
		}
	 }

}
