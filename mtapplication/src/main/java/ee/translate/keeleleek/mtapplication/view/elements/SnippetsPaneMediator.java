package ee.translate.keeleleek.mtapplication.view.elements;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.mediator.Mediator;

import ee.translate.keeleleek.mtapplication.Notifications;
import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;


public abstract class SnippetsPaneMediator extends Mediator {

	public final static String NAME = "{F008B792-2A8D-488C-8759-6FC94000AD4A}";
	
	
	protected boolean feedback = false;
	
	
	// INIT:
	public SnippetsPaneMediator() {
		super(NAME, null);
	}

	@Override
	public void onRegister()
	 {
		feedback = true;
		setSnippet(MinorityTranslateModel.preferences().getSnippet(getKeyIndex()));
		feedback = false;
	 }
	

	// NOTIFICATIONS
	@Override
	public String[] listNotificationInterests() {
		return new String[]
		{
			Notifications.ENGINE_SELECTED_ARTICLE_CHANGED,
		};
	}
	
	@Override
	public void handleNotification(INotification notification)
	 {
		feedback = true;
		
		switch (notification.getName()) {

		case Notifications.ENGINE_SELECTED_ARTICLE_CHANGED:
			break;

		default:
			break;
		}
		
		feedback = false;
	 }
	
	
	// INHERIT
	protected abstract int getKeyIndex();
	protected abstract String getSnippet();
	protected abstract void setSnippet(String snippet);
	
	
	// EVENTS
	public void onSelectedSnippetChanged(int i)
	 {
		feedback = true;
		setSnippet(MinorityTranslateModel.preferences().getSnippet(i));
		feedback = false;
	 }
	
	public void onSnippetChanged(int i, String snippet)
	 {
		MinorityTranslateModel.preferences().chnageSnippet(i, snippet);
	 }
	
}
