package ee.translate.keeleleek.mtapplication;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinorityTranslateFXLoader {
	
	public static void main(String[] args) throws IOException
	 {
		MinorityTranslate.setupRoot();
		
		Logging.setup();
		Logger logger = LoggerFactory.getLogger(MinorityTranslateFXLoader.class);
		logger.info(System.getProperty("java.vendor") + " " + System.getProperty("java.version"));
		logger.info(System.getProperty("os.name") + " " + System.getProperty("os.version"));
		
		// TODO: Resolve hack:
		try {
			loadFX();
			System.out.println(Class.forName("javafx.scene.control.Alert"));
		} catch (Throwable e) {
			logger.error("Failed to load JavaFX u40");
			JOptionPane.showMessageDialog(null, "MinorityTranslate requires Java 8u40 with JavaFX to run", "Newer Java required", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		MinorityTranslate.main(args);
	 }

	private static void loadFX() throws Exception
	 {
		String home = System.getProperty("java.home");
		File file = new File(home);
		file = new File(file, "lib");
		file = new File(file, "jfxrt.jar");
		
		System.out.println("Loading JavaFX from " + file + "!");
		
		Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
		method.setAccessible(true);
		method.invoke(ClassLoader.getSystemClassLoader(), new Object[] { file.toURI().toURL() });
	}

}
