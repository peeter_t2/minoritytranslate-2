package ee.translate.keeleleek.mtapplication.controller.content;

import org.puremvc.java.multicore.interfaces.INotification;
import org.puremvc.java.multicore.patterns.command.SimpleCommand;

import ee.translate.keeleleek.mtapplication.model.MinorityTranslateModel;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle;
import ee.translate.keeleleek.mtapplication.model.content.MinorityArticle.TitleStatus;
import ee.translate.keeleleek.mtapplication.model.content.Reference;
import ee.translate.keeleleek.mtapplication.view.dialogs.DialogFactory;
import ee.translate.keeleleek.mtapplication.view.messages.Messages;

public class ContentRequestUploadQidCommand extends SimpleCommand {

	@Override
	public void execute(INotification notification)
	 {
		String qid = (String) notification.getBody();
		if (qid == null) return;
		
		boolean toUpload = false;
		
		String[] langCodes = MinorityTranslateModel.preferences().getDstLangCodes();
		for (String langCode : langCodes) {
			
			Reference ref = new Reference(qid, langCode);
			
			MinorityArticle article = MinorityTranslateModel.content().getArticle(ref);
			
			String title = article.getTitle();
			String text = article.getText();
			TitleStatus titleStatus = article.getTitleStatus();
			boolean isNew = article.isNew();
			
			if (title == null || text == null) continue;
			if (title.isEmpty() && text.isEmpty()) continue; // not edited

			if (!article.isUploadRequired()) continue;
			
			if (title.isEmpty()) {
				DialogFactory.dialogs().showError(Messages.getString("messages.upload.request.failure.header"), Messages.getString("messages.upload.request.failure.content.missing.title"));
				toUpload = true;
				continue;
			}
			
			if (isNew && titleStatus != TitleStatus.OK) {
				if (titleStatus == TitleStatus.CONFLICT) DialogFactory.dialogs().showError(Messages.getString("messages.upload.request.failure.header"), Messages.getString("messages.upload.request.failure.content.title.in.use"));
				else DialogFactory.dialogs().showError(Messages.getString("messages.upload.request.failure.header"), Messages.getString("messages.upload.request.failure.content.title.not.checked"));
				toUpload = true;
				continue;
			}
			
			toUpload = true;
			
			MinorityTranslateModel.usage().createUsage(ref);
			
			MinorityTranslateModel.content().requestUpload(ref);
			
		}
		
		if (!toUpload) {
			DialogFactory.dialogs().showError(Messages.getString("messages.upload.request.failure.header"), Messages.getString("messages.upload.request.failure.content.no.edits.found"));
		}
		
	 }
	
}
